#include <stdio.h>
int fibonacci(int n);
int pattern(int y);
int i=0, r=0;

int fibonacci(int n)
{
	if (n==0) 
		return 0;
	else 
		if (n==1)
			return 1;
		else
			return (fibonacci(n-1) + fibonacci(n-2));
}

int pattern(int y)
{
	if (i<=y)
	{
		printf("%d \n", fibonacci(r));
		r=r+1;
		i=i+1;
		pattern(y);
	}
	else
		return 0;
}

int main()
{
	int x;
	printf("Enter a number:");
	scanf("%d", &x);
	pattern(x);
	return 0;
}
